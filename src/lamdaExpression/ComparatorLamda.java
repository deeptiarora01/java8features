package lamdaExpression;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

public class ComparatorLamda {
       public static void main(String args[]){
           
           Comparator<String> comp = new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return Integer.compare(s2.length(),s1.length());
                }
           };
                
        Comparator<String> compLambda=(String s1, String s2) -> Integer.compare(s2.length(),s1.length());
        List<String> list = Arrays.asList("*","**","***","*****","****");
        java.util.Collections.sort(list,compLambda);
        System.out.println(list);

        Consumer<String> c = s->System.out.println(s);
        
        Consumer<String> c1 = System.out::println;
        
        List<String> list1 =Arrays.asList("a","aa","aaaa","aaa");
        list1.forEach(System.out::println);
        
       }
       
       

}

