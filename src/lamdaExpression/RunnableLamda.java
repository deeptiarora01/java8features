package lamdaExpression;

public class RunnableLamda {

    public static void main(String[] args) throws InterruptedException {

        
        
        /*Runnable runnable = new Runnable(){
            @Override
            public void run() {
                for(int i=0;i<3;i++){
                    System.out.println(i +" Hello world for thread ["+ Thread.currentThread().getName()+"]");
                }
            }
        };*/

        
        Runnable runnableLamda = () ->{
            for(int i=0;i<3;i++){
                System.out.println(i +" Hello world for thread ["+ Thread.currentThread().getName()+"]");
            }
        };
        Thread t1 = new Thread(runnableLamda);
        Thread t2 = new Thread(runnableLamda);
        t1.start();
        t2.start();
        
        
        
        
        t1.join();

        
        
    }

}
