package lamdaExpression;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class StringTrimAndComparision {
    public static void main(String args[]){
        List<String> enterpriseNameRaw=new LinkedList<>();
        enterpriseNameRaw.add("abc");
        enterpriseNameRaw.add("");
        List<String> enterpriseName =  enterpriseNameRaw.stream().map(e->e.trim().toUpperCase()).collect(Collectors.toList());
        System.out.println(enterpriseName);
    }

}
