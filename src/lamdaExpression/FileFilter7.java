package lamdaExpression;

import java.io.File;
import java.io.FileFilter;

public class FileFilter7{
    public static void main(String args[]){
        
        
        JavaFileFilter fileFilter = new JavaFileFilter();
        File dir = new File("E:\\Workspace\\Java8Features\\src\\lamdaExpression");
        File[] javaFiles = dir.listFiles(fileFilter);
        
        
        for(File file:javaFiles){
            System.out.println(file.getName());
        }
    }
}



class JavaFileFilter implements FileFilter {
    @Override
    public boolean accept(File file) {
        return file.getName().endsWith(".java");
    }
}
