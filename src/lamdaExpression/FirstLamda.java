package lamdaExpression;
import java.io.File;
import java.io.FileFilter;


public class FirstLamda {
    public static void main(String[] args) {
        FileFilter filter = (File pathname) -> pathname.getName().endsWith(".java");
        File dir = new File("E:\\Workspace\\Java8Features\\src\\lamdaExpression");
        File[] files = dir.listFiles(filter);
        for(File f :files ){
            System.out.println(f);
        }
    }
}

