package functionalInterfaces;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {
    public static void main(String args[]) {
        Consumer<String> c = x -> System.out.println(x);
        boolean flag =true;
        int i;
        System.out.println("Git Hook");
        System.out.println("Git Hook");
        List<String> strings = Arrays.asList("five", "four", "three","two","one");       
        if(flag)
            System.out.println("testing rule");
        strings.forEach(c);
    }
}
